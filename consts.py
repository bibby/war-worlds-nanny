class Ship:
	"""
	Ship type constants
	"""

	def __init__(self):
		pass

	TROOP = "troopcarrier"
	COLONY = "colonyship"
	FIGHTER = "fighter"
	SCOUT = "scout"
	WORMHOLE = "wormhole-generator"


class Building:
	"""
	Building type constants
	"""

	def __init__(self):
		pass

	SILO = "silo"
	BIO = "biosphere"
	SHIELD = "groundshield"
	SHIP = "shipyard"
	RADAR = "radar"
	RESEARCH = "research"

	max_levels = {
		"silo": 5,
		"biosphere": 4,
		"groundshield": 5,
		"shipyard": 2,
		"radar": 5,
		"research": 1
	}


class Resource:
	MINE = "mine"
	FOOD = "food"
	POP = "pop"
	BUILD = "build"