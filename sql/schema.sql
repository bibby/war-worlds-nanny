create table if not exists stars(
    id int primary key,
    name varchar(16),
	type int,
    x float,
    y float,
    planets int,
    colonies int,
    defense float,
    fighters float,
    troops float,
    colonyships float,
    max_pop float,
    max_food float,
    player int default 0
);

create index if not exists stars_x on stars(x);
create index if not exists stars_y on stars(y);
create index if not exists stars_player on stars(player);

create table if not exists actions(
    id int primary key,
    fighters_sent float default 0,
    troops_sent float default 0,
    colonyships_sent float default 0,
	scouted boolean default 0
);

create table if not exists alliances(
    id int primary key,
    name varchar(64),
    member_count int,
    bank int
);

create table if not exists players(
    id int primary key,
    name varchar(64),
    alliance int,
    rank int,
    stars int,
    colonies int,
    ships int,
    population int
);
