select
	stars.id,
	stars.name,
	stars.x,
	stars.y,
	stars.defense
from (stars, actions)
where stars.id = actions.id
and %s
