import logging
from config import Config
from db import Db
from war_nanny import API

api = API()
Config.log["level"] = logging.DEBUG
Db.init()
api.authenticate()


for a in api.get_alliances():
	if a.member_count < 10:
		continue
	member_ids = [m for m in api.get_alliance_members(a.id)]
	map(Db.update_player, api.get_players(member_ids))