import sys
from db import Db
from nav import Mapper
from war_nanny import API

arg_names = ['command', 'x', 'y', 'range']
args = dict(zip(arg_names, sys.argv))

if not args.get("y") or not args.get("x"):
	print "python star_scan.py {x} {y} {range}"
	exit(1)

sector = (int(args.get("x")), int(args.get("y")))
radius = int(args.get("range")) or 0
radius = max(radius, 0)
radius = min(radius, 15)

api = API()
api.authenticate()
Db.init(silent=False)

Mapper.map_sectors(Db.add_stars, api, sector, radius)
