# War Nanny

A sitter for [War Worlds](http://www.war-worlds.com), a game for android.

After colonizing a world, build one biosphere and let the nanny do the rest.

- Manages a structure upgrade pipeline
- - biospheres
- - ground shields
- - shipyards
- - silos

- Manages resource allocations
- - effecient food productions
- - priority on population growth
- - switches to mining and building

- Builds ships!
- - incremental orders for small fleets
- - merges with a main

