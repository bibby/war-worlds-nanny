"""
War Nanny - The baby-sitter for War Worlds on Android.
Main Startup script.

Usage:
	main.py --version
	main.py [ -hvn TIMES | --version | --times TIMES]
		[ --all-tasks | --no-tasks ]
		[ --resources | --no-resources ]
		[ --upgrades | --no-upgrades ]
		[ --military | --no-military ]
		[ --attack | --no-attack ]
		[ --navigation | --no-navigation ]
		[ --rush | --no-rush ]

Options:
	-h --help		Show this screen
	-v --version	Display nanny version
	-n				Times to run (omit for continuous run)
"""
from db import Db
from war_nanny import WarNanny
from docopt import docopt

if __name__ == '__main__':
	version = 'WWNanny 0.140614 [Dalby]'
	arguments = docopt(__doc__, version=version)
	if arguments.get("-v"):
		print version
		exit(0)

	tasks = ["resources", "upgrades", "attack", "military", "navigation"]
	overrides = {}
	if arguments.get("--all-tasks"):
		overrides = {t: False for t in tasks}
	if arguments.get("--no-tasks"):
		overrides = {t: False for t in tasks}

	for t in tasks:
		yes = "--%s" % t
		no = "--no-%s" % t
		if arguments.get(yes):
			overrides[t] = True
		if arguments.get(no):
			overrides[t] = False

	toggles = {}
	if arguments.get("--rush"):
		toggles["rush_enabled"] = True

	if arguments.get("--no-rush"):
		toggles["rush_enabled"] = False

	run_limit = arguments.get("TIMES")

Db.init()
WarNanny().start(run_limit=run_limit, tasks=overrides, toggles=toggles)
