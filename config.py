"""
War Nanny configuration class
"""
import logging
from random import random
import time
from consts import Ship, Building


class Config:
	# logging vars
	log = {
		"level": logging.INFO,
		"file": "nanny.log"
	}

	# toggles for nanny behavior
	tasks = {
		"authenticate": True,
		"resources": True,
		"upgrades": True,
		"military": True,
		"attack": True,
		"navigation": True
	}

	# during the run, be nice to the server and wait?
	# time in seconds, and may be floats
	delays = {
		"between_runs": lambda: (5 * random() + 3) * 60,
		"between_requests": lambda: 2 * random() + 1,
		"bad_gateway": 15
	}

	# player data
	player = {
		"id": "",
		"sector": (0, 0),
		"coords": (0.00, 0.00),
		"alliance": [],
		"allies": [],
	}

	# shapes of 3 or more point, where ships should not go
	borders = [
		# [(92.45, 5.05), (91.13, 5.45), (91.60, 6.05), (91.07, 5.81)]
	]

	# buildings
	pipeline = [
		(Building.BIO, 3),
		(Building.RESEARCH, 1),
		(Building.BIO, 4),
		(Building.SHIP, 1),
		(Building.SHIELD, 5)
	]

	# rushing
	rush_enabled = True
	rush = [Building.BIO, Building.RESEARCH]
	solo_rush = [Building.SHIP, Ship.COLONY]

	# resource allocations
	resources = {
		# after determining the food needed to survive,
		# multiply by this to get the food *desired*
		"food_safety": 1.02,

		# farm worlds may use, at most, this share of the population (1.0)
		"max_farm_pop": 0.85,

		# when building, use this share of the population (1.0)
		"build_pop": 0.15
	}

	ships = {
		Ship.FIGHTER: {
			"build": 50,
			"build_max": None,
			"navigate": False,
			"merge": True,
			"max_send": 500,
			"min_send": 70,
			"max_dist": 40,
			"tether": 50
		},
		Ship.TROOP: {
			"build": 50,
			"build_max": 1000,
			"navigate": True,
			"merge": True,
			"max_send": 500,
			"min_send": 1,
			"max_dist": 50
		},
		Ship.COLONY: {
			"build": 1,
			"build_max": 1,
			"navigate": False,
			"merge": False,
			"max_send": 1,
			"min_send": 1,
			"max_dist": 40
		},
		Ship.SCOUT: {
			"build": 0,
			"build_max": 1,
			"navigate": False,
			"merge": False,
			"max_send": 1,
			"min_send": 1,
			"max_dist": 100
		}
	}

    # sys
	google = {
		"url": "https://android.clients.google.com/auth",
		"form": {
			# NEED!  fill in all missing fields!
			"accountType": "",
			"Email": "",
			"has_permission": 1,
			"Token": "",
			"service": "oauth2:email",
			"source": "android",
			"androidId": "",
			"app": "au.com.codeka.warworlds",
			"client_sig": "",
			"device_country": "",
			"operatorCountry": "",
			"lang": "",
			"sdk_version": 17
		},
		"headers": {
			"User-Agent": "GoogleLoginService/1.3 (espresso10wifi JDQ39)",
			"Connection": "Keep-Alive",
			"Content-Type": "application/x-www-form-urlencoded"
		}
	}

	retry = {
		"auth-retry": 100,
		"auth-wait": 15
	}

	# developer options
	debug = {
		# when True, last response is written to <response-file>
		"save_responses": True,
		# file to write to
		"response-file": "debug-response",

		"save-requests": True,
		# file to write to
		"request-file": "debug-request"
	}

	# toggle use of mock data
	mock = {
		"auth": False,
		"http": False,
		"empire": False
	}

	# game client user agent
	client = {
		"agent": "wwmmo/0.4.786"
	}

	# game server domain
	server = {
		"domain": "https://game.war-worlds.com"
	}
