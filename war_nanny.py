"""
	War-Worlds nanny
	Authenticates with google using user-captured parameters
	located in config.py, and creates a game session.
	Then, we request empire data and make appropriate modifications
	with outgoing http requests

	@author bibby <bibby@bbby.org>
"""

import logging
import re
import time
import traceback
import itertools
import requests
import sys
from requests.exceptions import Timeout
from config import Config
from domain import Empire, Sector, Alliance
from random import random

import warworld_pb2

# TODO, @see issue #20, let's be pro and have per-module loggers and shared config. logging module?
logging.basicConfig(
	filename=Config.log.get("file", "nanny.log"),
	level=Config.log.get("level", logging.DEBUG),
	format='%(asctime)s.%(msecs)d %(levelname)s %(module)s - %(funcName)s: %(message)s',
	datefmt="%Y-%m-%d %H:%M:%S"
)

class NannyException(Exception):
	pass


class WarNanny():
	"""
	War Worlds game nanny
	"""
	def __init__(self):
		"""
		Initialize the game sitter with an empty empire,
		and a default state
		"""
		self.empire = None
		self.run = False

	def start(self, **kwargs):
		"""
		Run the sitter, if it isn't already
		"""
		self.validate_config()

		config_override = kwargs.get("tasks")
		if config_override:
			for k in config_override:
				Config.tasks[k] = config_override[k]

		run_limit = kwargs.get("run_limit")
		if run_limit:
			run_limit = int(run_limit)
		
		toggles = kwargs.get("toggles")
		if toggles:
			if "rush_enabled" in toggles:
				Config.rush_enabled = toggles.get("rush_enabled", False)
			

		if self.run is True:
			logging.warn("nanny already running!")
			return False

		self.run = True
		# run until stopped.
		logging.info("Starting Nanny")
		try:
			self.main_loop(run_limit)
		except Exception as ex:
			logging.error(ex)
			lines = traceback.format_exception(*sys.exc_info())
			logging.error(''.join('!! ' + line for line in lines))
		finally:
			self.stop()

		logging.info("Stopping Nanny")

	def stop(self):
		"""
		Instruct the sitter to stop
		"""
		self.run = False

	def validate_config(self):
		player_id = Config.player.get("id")
		form = Config.google.get("form")
		android_id = form["androidId"]
		email = form["Email"]

		if not all([player_id, android_id, email]):
			raise NannyException("Nanny is not properly configured. Fill out config.py!")

	def auth_and_load(self, api):
		# real or mock auth, as desired
		if Config.tasks.get("auth", True):
			logging.debug("authenticating..")
			api.authenticate()

		api.load_alliance()

		# load real or mock empire
		logging.debug("loading empire..")
		self.empire = Empire()
		api.load_empire(self.empire)

	def main_loop(self, run_limit=None):
		"""
		As long as the process continues, it will remain in
		this core loop until stopped (by exception or pkill)
		Runnable tasks may be toggled on/off via config
		"""
		logging.debug("entering main loop")

		# http game client
		api = API()

		while self.run:
			auth_retries = Config.retry.get("auth-retry", 5)
			auth_retry_wait = Config.retry.get("auth-wait", 60)
			authenticated = False
			if isinstance(run_limit, (int, long)) and run_limit == 0:
				logging.info("run limit reached. exiting.")
				return

			while not authenticated:
				try:
					self.auth_and_load(api)
					authenticated = True
				except Exception as ex:
					logging.error(ex)
					lines = traceback.format_exception(*sys.exc_info())
					logging.error(''.join('!! ' + line for line in lines))
					logging.info("Retrying up to %d more times after a %d sec pause.." % (auth_retries, auth_retry_wait))
					time.sleep(auth_retry_wait)
					auth_retries -= 1
					if auth_retries < 0:
						raise NannyException("auth and load hopelessly broken.")

			if Config.tasks.get("resources", True):
				logging.info("running resources")
				self.empire.resources(api)

			if Config.tasks.get("upgrades", True):
				logging.info("running upgrades")
				self.empire.upgrade(api)

			if Config.tasks.get("military", True):
				logging.info("running military")
				self.empire.military(api)

			if Config.tasks.get("navigation", True):
				logging.info("running navigation")
				self.empire.navigate(api)

			if Config.tasks.get("attack", True):
				logging.info("running attack")
				self.empire.attack(api)

			# take a break before the next run
			self.empire = None
			time_fn = Config.delays.get("between_runs", lambda: 5*60)
			sleep_time = time_fn()
			logging.info("resting.. %f", sleep_time)
			time.sleep(sleep_time)

			if run_limit > 0:
				run_limit -= 1

		logging.debug("leaving main loop")
		pass


class API:
	"""
	HTTP Client to the game server.
	Communicates mainly using protocol buffers over SSL,
	in a RESTful style
	"""
	player = Config.player.get("id")
	error_count = 0

	def __init__(self, player_override=None):
		self.token = None
		self.session = None
		self.empire = None
		self.player_override = player_override

		self.game_url = Config.server.get("domain", "https://game.war-worlds.com")

		# common request headers
		self.headers = {
			"User-Agent": Config.client.get("agent", "wwmmo/0.4.783"),
			"Accept": "application/x-protobuf",
			"Content-Type": "application/x-protobuf"
		}

	def authenticate(self):
		"""
		Get authentication tokens from
		google and the game server
		"""
		if Config.mock["auth"]:
			logging.info("using mock auth")
			self.token = "mock-token"
			self.session = "mock-session"
		else:
			self.auth_google()
			self.auth_game()

		if self.player_override:
			API.player = self.player_override

	def auth_google(self):
		"""
		Get authentication token from google
		"""
		logging.debug("auth google")
		self.token = None

		goog = Config.google
		url = goog.get("url")
		# todo privatize data?
		form = goog.get("form")
		headers = goog.get("headers")

		response = requests.post(url, data=form, headers=headers, verify=False)
		for line in response.content.split("\n"):
			matches = re.match("^Auth=(?P<token>.*)$", line)

			if matches and matches.group("token"):
				self.token = matches.group("token")
				break

		logging.info("google token %s", self.token)
		if not self.token:
			raise NannyException("could not get auth token")

	def auth_game(self):
		"""
		Exchanges google token for a game session
		"""
		logging.debug("auth game")

		auth_url = "%s/realms/beta/login" % self.game_url
		payload = {"authToken": self.token}
		logging.debug("url %s", auth_url)

		req = requests.get(auth_url, params=payload)
		self.session = req.text
		logging.info("session %s", self.session)

		# todo better test. session could be anything truthy
		if not self.session:
			raise NannyException("could not get session")

	def load_alliance(self):
		alliances = Config.player.get("alliance")
		if not alliances:
			return None

		allies = Config.player.get("allies") or []
		for alliance_id in list(alliances):
			alliance = self.get_alliance(alliance_id)
			members = [m.id for m in alliance.members]
			allies += map(str, members)
			logging.info("add allies %s[%d]" % (alliance_id, len(members)))
		Empire.allies = allies
		logging.info("allies = %d" % len(Empire.allies))

	def load_empire(self, empire):
		"""
		Unpack a protobuf with our empire data.
		Could be file 'mock-empire' if mocked,
		else we'll ask the game.
		"""

		if Config.mock.get("empire", False):
			logging.info("using mock empire")
			mock_file = open("mock-empire")
			star_response = mock_file.read()
		else:
			url = "/realms/beta/empires/%s/stars" % API.player
			star_response = self.game_request(url, method="GET")

		# unpack protobuf as a StarResponse message
		empire_proto = warworld_pb2.StarResponse()
		empire_proto.ParseFromString(star_response)

		if empire_proto is None:
			raise NannyException("Failed to load empire from proto")

		empire.load_from_proto(empire_proto)

		# keep a local reference to aid in log messages (star names, etc)
		self.empire = empire

	def load_sectors(self, sector_list):
		"""
		Makes a request for sectors.
		Normal nanny operations should not need this;
		Is useful for building the nav db
		"""
		sector_list = itertools.ifilter(None, sector_list)
		sector_arg = "|".join([",".join([str(x), str(y)]) for x, y in sector_list])
		if not sector_arg:
			return None

		params = {"coords": sector_arg}

		content = self.game_request("/realms/beta/sectors", "GET", params=params)
		sector_response = warworld_pb2.SectorsResponse()
		sector_response.ParseFromString(content)

		return map(Sector, sector_response.sectors)


	def game_request(self, uri, method="POST", data=None, params=None):
		"""
		Common passthru for requests to the game server.
		Tests return status, and logs result to
		"""
		url = self.game_url + uri

		if Config.mock.get("http"):
			logging.info("MOCK request: %s" % url)
			return True

		if data:
			if Config.debug.get("save-requests", False):
				debug_file = open(Config.debug.get("request-file", "debug-request"), "wb")
				debug_file.write(data)
				debug_file.close()

		try:
			response = requests.request(method, url,
				cookies={"SESSION": self.session},
				headers=self.headers,
				data=data,
				params=params,
				timeout=45
			)
		except Timeout as e:
			logging.error(e.message)
			return None

		if response.status_code == 200:
			logging.debug("Success")
		elif response.status_code == 502:
			# this tends to happen; poor guy. It usually clears up after a while
			sleep_time = Config.delays.get("bad_gateway", 15)
			logging.warn("502 Bad gateway. Sleeping it off.. %d sec" % sleep_time)
			time.sleep(sleep_time)
		else:
			logging.warn("Request %s returned status %d" % (url, int(response.status_code)))
			API.error_count += 1
			if API.error_count > 4:
				raise NannyException("Err limit reached. bailing.")

		# if desired, write responses to a debug file
		# for later offline inspection
		if Config.debug.get("save-responses", False):
			debug_file = open(Config.debug.get("response-file", "debug-response"), "wb")
			debug_file.write(response.content)
			debug_file.close()

		# be a pal and not hammer the server
		self.nice()

		return response.content

	def nice(self):
		"""
		We'll be nice to the poor game server and add some
		rest time between web requests
		"""
		wait_fn = Config.delays.get("between_requests", lambda t: 2 * random() + 1)
		time.sleep(wait_fn())

	def upgrade_building(self, building_type, colony, id=""):
		"""
		Order a planet structure upgrade or build a new one.
		When id is omitted, a new structure is made.
		"""
		log_vars = (
			building_type,
			id,
			self.empire.star_name(colony.data.star),
			str(colony.data.order)
		)

		logging.info("UPGRADE %s[%s] on %s %s " % log_vars)

		req = self.build_building_request(warworld_pb2.BUILDING, building_type, colony, id)
		return self.game_request("/realms/beta/buildqueue", data=req.SerializeToString())

	def order_ships(self, colony, ship_type, count=1):
		"""
		Order a new fleet of ships.
		"""
		log_vars = (
			count,
			ship_type,
			self.empire.star_name(colony.data.star),
			str(colony.data.order)
		)

		logging.info("SHIP ordered %d x %s on %s %s " % log_vars)

		req = self.build_building_request(warworld_pb2.SHIP, ship_type, colony, count=count)
		return self.game_request("/realms/beta/buildqueue", data=req.SerializeToString())

	def build_building_request(self, build_type, item_type, colony, id="", count=1):
		"""
		Build up a BuildRequest protobuf message
		"""
		req = warworld_pb2.BuildRequest()
		req.build_type = build_type
		req.player = API.player
		req.colony = colony.data.id
		req.type = item_type
		req.count = count
		req.id = id
		req.star = colony.data.star

		return req

	def rush_build(self, building_type, colony, job_id, amount=1.0):
		log_vars = (
			building_type,
			job_id,
			self.empire.star_name(colony.data.star),
			str(colony.data.order)
		)

		if not job_id:
			logging.warn("invlaid job id for %s[%s] on %s %s " % log_vars)
			return None

		logging.info("RUSH %s[%s] on %s %s " % log_vars)
		params = {"amount": "%1.2f" % amount}
		url = "/realms/beta/stars/%s/build/%s/accelerate" % (colony.data.star, job_id)
		return self.game_request(url, params=params)

	def resource_request(self, colony, resources):
		"""
		Modify a Colony protobuf message with new
		resource allocations and PUT it back on the server.

		This function may return None if the new values
		are not sufficiently departed from their originals.
		"""
		url = "/realms/beta/stars/%s/colonies/%s" % (colony.star, colony.id)
		star = self.empire.star_name(colony.star)
		order = colony.order

		if abs(colony.focus_pop - resources["pop"]) < 0.01 \
			and abs(colony.focus_food - resources["food"]) < 0.01 \
			and abs(colony.focus_build - resources["build"]) < 0.01:
			logging.debug("skipping resources on %s %d. No significant change" % (star, order))
			return None

		colony.focus_pop = resources["pop"]
		colony.focus_food = resources["food"]
		colony.focus_mine = resources["mine"]
		colony.focus_build = resources["build"]
		logging.info("RESOURCE on %s %d to %s" % (star, order, resources))

		return self.game_request(url, method="PUT", data=colony.SerializeToString())

	def merge_fleets(self, star_id, ship_type, fleet_a, fleet_b):
		"""
		Given two fleets (_a and _b), merge them into one.
		This call takes place when two fleets of the same ship type
		are idle around the same star during a nanny run.
		"""
		star_name = self.empire.star_name(star_id)
		logging.info("MERGE %s on %s" % (ship_type, star_name))

		uri = self.fleet_uri(star_id, fleet_a)
		req = warworld_pb2.FleetRequest()
		req.order = warworld_pb2.MERGE
		req.merge_target = fleet_b

		return self.game_request(uri, data=req.SerializeToString())

	def split_fleet(self, star_id, ship_type, fleet, new_size=1):
		"""
		Separate a fleet into two.
		The breakout number is the size of the newly created fleet.
		The original fleet remains at a reduced size.
		"""
		breakout = int(max(fleet.count - new_size, 1))
		remain = int(fleet.count - breakout)
		if remain <= 0:
			return None

		star_name = self.empire.star_name(star_id)
		logging.info("SPLIT %s on %s: %f and %f" % (ship_type, star_name, fleet.count - breakout, breakout))

		uri = self.fleet_uri(star_id, fleet.id)
		req = warworld_pb2.FleetRequest()
		req.order = warworld_pb2.SPLIT
		req.split_remain = remain
		req.split_new = breakout
		fleet.count -= breakout

		return self.game_request(uri, data=req.SerializeToString())

	def move_fleet(self, fleet, origin, target):
		"""
		Move a fleet to a new location.
		This costs money!
		"""
		origin_name = self.empire.star_name(origin)
		logging.info("MOVE %f x %s from %s to %s" % (fleet.count, fleet.type, origin_name, target.get("name")))

		star_id = target.get("id")
		uri = self.fleet_uri(origin, fleet.id)

		req = warworld_pb2.FleetRequest()
		req.order = warworld_pb2.MOVE
		req.move_target = unicode(star_id)
		
		return self.game_request(uri, data=req.SerializeToString())

	def get_star(self, star_id):
		"""
		Request star data from a server.
		The game client typically does this after every change.
		"""
		url = self.star_url(star_id)
		return self.game_request(url, "GET")

	def get_players(self, player_list):
		response = self.game_request("/realms/beta/empires/search", "GET", params={"ids": ",".join(player_list)})
		proto = warworld_pb2.PlayerList()
		proto.ParseFromString(response)
		return proto.players

	def get_alliance_members(self, alliance_id):
		return [m.id for m in self.get_alliance(alliance_id).members]

	def get_alliance(self, alliance_id):
		response = self.game_request("/realms/beta/alliances/%s" % alliance_id, "GET")
		proto = warworld_pb2.Alliance()
		proto.ParseFromString(response)
		return Alliance(proto, record=False)

	def get_alliances(self):
		response = self.game_request("/realms/beta/alliances", "GET")
		proto = warworld_pb2.AlliancesResponse()
		proto.ParseFromString(response)
		return map(Alliance, proto.alliances)


	def star_url(self, star_id):
		"""
		Fetches a star's data
		"""
		return "/realms/beta/stars/%s" % star_id

	def fleet_uri(self, star_id, fleet):
		"""
		Generate a fleet resource locator
		"""
		return "/realms/beta/stars/%s/fleets/%s/orders" % (star_id, fleet)

	def attack_colony(self, colony):
		"""
		Sends a request to attack a colony
		"""
		logging.info("ATTACK colony on %s %d" % (self.empire.star_name(colony.data.star), colony.data.order))
		url = self.attack_url(colony)
		return self.game_request(url)

	def attack_url(self, colony):
		"""
		Generates a url for a colony attack request
		"""
		return "/realms/beta/stars/%s/colonies/%s/attack" % (colony.data.star, colony.data.id)

	def colonize(self, star_id, order):
		"""
		Sends a request to colonize a world
		"""
		star_name = self.empire.star_name(star_id)
		logging.info("COLONIZE %s %d" % (star_name, order))

		url = self.colonize_url(star_id)
		req = warworld_pb2.ColonizeRequest()
		req.order = order
		return self.game_request(url, data=req.SerializeToString())

	def colonize_url(self, star_id):
		"""
		Generates a url for a new colonization
		"""
		return "/realms/beta/stars/%s/colonies" % star_id
