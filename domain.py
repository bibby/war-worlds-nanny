"""
Domain objects for War Nanny.
These are generally behavioral wrappers for
protocol buffer messages.
"""
import logging
from config import Config
from db import Db
from nav import Mapper
from consts import Ship, Building, Resource
import warworld_pb2


class Empire:
	allies = []

	"""
	The player's collection of stars, colonies, and fleets
	"""
	def __init__(self):
		self.stars = {}
		self.mapper = Mapper()

	def load_from_proto(self, proto):
		"""
		Instantiate Star objects and index them
		in a dictionary by id.
		"""
		self.stars = {star_id: Star(data, record=True) for (star_id, data) in [(star.id, star) for star in proto.stars]}
		logging.info("Summary: %s" % self.summary())

	def summary(self):
		star_count = 0
		colony_count = 0
		ship_count = 0
		pop_count = 0
		for star_id in self.stars:
			star = self.stars[star_id]
			colonies = len(star.colonies)
			if colonies:
				star_count += 1
				colony_count += colonies
				for colony in star.colonies:
					pop_count += colony.data.cur_pop
			for fleet in star.fleets:
				ship_count += fleet.count

		return {
			"stars": star_count,
			"colonies": colony_count,
			"ships": ship_count,
			"population": pop_count,
		}

	def reload_star(self, api, star_id):
		"""
		Fetch and replace data for a star
		"""
		star_name = self.star_name(star_id)
		logging.info("reloading data for %s" % star_name)

		try:
			data = api.get_star(star_id)
			star_proto = warworld_pb2.Star()
			star_proto.ParseFromString(data)
			self.stars[star_id] = Star(star_proto, record=True)
		except:
			logging.error("reload of %s failed" % star_name)

	def star_name(self, star_id):
		"""
		Return the name of a star from its id
		"""
		star = self.stars[star_id]
		return star.data.name

	def upgrade(self, api):
		"""
		Check each star for nominated building upgrades
		or new structure creation.
		"""
		for star_id in self.stars:
			for building_type, colony, id, rush in self.stars[star_id].assess_upgrades() or []:
				if rush:
					api.rush_build(building_type, colony, id)
				else:
					api.upgrade_building(building_type, colony, id)

	def resources(self, api):
		"""
		Check each star for resource allocation adjustments
		"""
		for star_id in self.stars:
			for colony, res in self.stars[star_id].assess_resources() or []:
				api.resource_request(colony.data, res.to_floats())

	def military(self, api):
		"""
		Check each star for fleet creations or mergers
		"""
		for star_id in self.stars:
			should_reload = False
			for colony, orders in self.stars[star_id].assess_military() or []:
				for order in orders:
					api.order_ships(colony, *order)
					should_reload = True

			for merge in self.stars[star_id].assess_merges() or []:
				api.merge_fleets(star_id, *merge)
				should_reload = True

			if should_reload:
				self.reload_star(api, star_id)

	def attack(self, api):
		"""
		Check each star for planets with colonies that do not
		belong to the player. If there are enough troopcarriers
		to take it, then take it.
		"""
		for star_id in self.stars:
			colony = self.stars[star_id].assess_attack()
			if colony:
				api.attack_colony(colony)

			planets = self.stars[star_id].assess_colonize()
			if planets:
				for planet in planets:
					api.colonize(star_id, planet.order)

	def navigate(self, api):
		"""
		Take a look at the star's fleets to see if
		they're better served elsewhere, and go there.
		"""
		for star_id in self.stars:
			star = self.stars.get(star_id)
			moves = star.assess_navigate(self.mapper, api)
			if moves:
				for fleet, target in moves:
					Db.send_callback.get(fleet.type)(fleet.count, target.get("id"))
					api.move_fleet(fleet, star_id, target)



class Star:
	"""
	Domain object for a solar system of several planets
	that may or may not have colonies belonging to player,
	somebody else, or no one
	"""

	ship_defaults = {
		"build": 0,
		"build_max": 1,
		"navigate": False,
		"merge": False
	}

	target_fns = [
		(Ship.FIGHTER, Db.fighter_target),
		(Ship.TROOP, Db.troop_target),
		(Ship.COLONY, Db.colony_target),
		(Ship.SCOUT, Db.scout_target)
	]

	def __init__(self, star, record=False):
		"""
		Initialize with an empty colony list
		and a Star protobuf message
		"""
		self.data = star

		self.colonies = []
		self.friendly_colonies = []
		self.enemy_colonies = []

		self.fleets = []
		self.friendly_fleets = []
		self.enemy_fleets = []

		self.planets = [p for p in star.planets]
		self.colonyship_handled = False

		self.load_colonies()
		self.load_fleets()

		if record:
		 	Db.update_star(self.summary())

	def get_name(self):
		"""
		The star's name
		"""
		return self.data.name

	def get_coords(self):
		"""
		The star's position
		"""
		x = self.data.sector_x + (float(self.data.sector_x_precision) / 1000)
		y = self.data.sector_y + (float(self.data.sector_y_precision) / 1000)
		return x, y

	def get_build_queue(self):
		"""
		Get a list of structures and fleets under construction
		"""
		return self.data.queue

	def load_colonies(self):
		"""
		Organize colonies between the player's and others',
		attaching building and queue info that strangely associated with the star
		"""
		colony_map = {}

		# Build queues and buildings are given to us as properties
		# of the star, despite having colony ids attached.
		# We'll map them to their Colony object for easier use later
		for c in self.data.colonies:
			colony_map[c.id] = {"colony": c, "queue": [], "buildings": [], "planet": self.get_planet_at(c.order)}

		for q in self.data.queue:
			if q.colony in colony_map:
				colony_map[q.colony]["queue"].append(q)

		for b in self.data.buildings:
			if b.colony in colony_map:
				colony_map[b.colony]["buildings"].append(b)

		player = Config.player.get("id")
		colonies = map(Colony, colony_map.values())
		self.colonies = [c for c in colonies if c.data.player == player]
		if len(self.colonies) == 1:
			self.colonies[0].is_solo = True

		self.friendly_colonies = set([c for c in colonies if c.data.player in Empire.allies])

		self.enemy_colonies = set(colonies) - set(self.colonies) - set(self.friendly_colonies)

	def load_fleets(self):
		"""
		Organize fleets between player's and others'
		"""
		player = Config.player.get("id")
		for fleet in self.data.fleets:
			if fleet.player == player:
				self.fleets.append(fleet)
			else:
				self.enemy_fleets.append(fleet)

	def get_empty_planets(self):
		"""
		Planets that can be colonized, no problem
		"""
		planets = [p.order for p in self.data.planets]
		taken = [c.order for c in self.data.colonies]
		empty = set(planets) - set(taken)
		return map(self.get_planet_at, list(empty))

	def assess_upgrades(self):
		"""
		Collect our colonies' building job nominations
		"""
		jobs = []
		for c in self.colonies:
			jobs += c.nominate_build_jobs() or []

		return jobs

	def assess_resources(self):
		"""
		Collect out colonies' resource allocation adjustments
		"""
		# short circuit
		if not self.colonies:
			return None

		## rolling sums
		# population
		total_pop = 0.0
		# best-case food production
		max_food = 0.0

		# temporary list for later sorting
		collection = []
		for c in self.colonies:
			total_pop += c.data.cur_pop

			# how good the planet is at food production
			congeniality = self.get_planet_at(c.data.order).cong_food
			max_value = c.data.cur_pop * congeniality / 100
			max_food += max_value

			collection.append([congeniality, max_value, c])

		# 100 people need 10 food per hour
		food_need = total_pop / 10

		# advised multiplier for food to make
		food_need *= Config.resources.get("food_safety", 1)

		# when non-farms have things to build, use this amount
		build_pop = Config.resources.get("build_pop", 0.15)

		# return object for new allocations
		resources = []

		# log gloomy predictions, though conditions may change
		if food_need > max_food:
			logging.warn("%s[%s] cannot possibly feed itself" % (self.data.name, self.data.id))
			for data in collection:
				congeniality, max_value, colony = data
				pop = colony.data.cur_pop
				r = ResourceAllocation(pop)
				r.allocate_remaining(Resource.FOOD)
				colony.focus = Resource.FOOD
				resources.append((colony, r))
			return resources

		# sort colonies as best candidates as farms
		collection = sorted(collection, key=lambda p: int(p[0]), reverse=True)

		for data in collection:
			congeniality, max_value, colony = data
			pop = colony.data.cur_pop
			pop_maxed = pop - colony.data.max_pop == 0
			r = ResourceAllocation(pop)

			if food_need:
				if max_value > 0:
					# take the lesser of what it takes to hit our target
					# or a configured max population allocation.
					# Having some pop left over to breed is the best way out of this mess.
					farm_pop = Config.resources.get("max_farm_pop", 0.8)
					if pop_maxed:
						farm_pop = 1.0
					food_share = min(farm_pop, food_need / max_value)
				else:
					# some rocks can't grow food.
					food_share = 0.0

				# people needed to grow the amount of food we want
				pop_needed = pop * food_share
				# and the food they'll produce
				food_produced = pop_needed * congeniality / 100
				# subtracted from the overall need
				food_need -= food_produced
				# and normalized, because floats.
				food_need = max([food_need, 0])
				if food_need < 1:
					food_need = 0

				colony.focus = Resource.FOOD
				r.allocate(Resource.FOOD, pop_needed)

			# population takes priority
			if not pop_maxed:
				if r.allocations[Resource.FOOD] == 0:
					colony.focus = Resource.POP
				r.allocate_remaining(Resource.POP)
			elif not colony.focus:
				# the rest of you to the mines!
				colony.focus = Resource.MINE

			# allow existing queues to continue
			if colony.queue:
				r.allocate(Resource.BUILD, pop * build_pop)
			
			r.allocate_remaining(Resource.MINE)
			resources.append((colony, r))

		self.elect_factory(resources)

		return resources

	@staticmethod
	def elect_factory(resources):
		# candidate planets
		mines = [(c, r) for (c, r) in resources if c.focus == Resource.MINE]
		if not mines:
			return

		mines = sorted(mines, key=lambda t: t[0].get_ratio(Resource.POP, Resource.MINE), reverse=True)
		colony, resources = mines.pop(0)
		colony.focus = Resource.BUILD
		resources.reset()
		resources.allocate_remaining(Resource.BUILD)

	def assess_military(self):
		"""
		Collect our colonies' ship orders
		"""
		ls = []
		for c in self.colonies:
			ls += c.nominate_ships(self) or []

		return ls

	def assess_merges(self):
		"""
		Collect any fleet mergers to occur
		"""
		if not len(self.data.fleets):
			return None

		ls = []
		for ship_type in Config.ships:
			cfg = Config.ships.get(ship_type, Star.ship_defaults)
			merge = cfg.get("merge", False)
			if not merge:
				continue

			fleets = sorted(self.get_idle_fleets(ship_type), key=lambda f: int(f.count), reverse=True)
			fleets = [f.id for f in fleets]

			if len(fleets) > 1:
				top_fleet = fleets.pop(0)
				while len(fleets):
					ls.append((ship_type, top_fleet, fleets.pop(0)))

		return ls

	def get_planet_at(self, order):
		"""
		Planets are named after their star:  Foo 1, Foo 2, etc
		The planet's "order" is the closest to an id we have.
		Given a numeric order, return the planet.
		"""
		for p in self.data.planets:
			if p.order == order:
				return p
		return None

	def assess_attack(self):
		"""
		Look for enemy colonies; If we have the troop ships
		to take it out, go for it
		"""
		e = sorted(self.enemy_colonies, key=lambda c: c.get_defense())
		if not e:
			return None

		troop_count = sum([fleet.count for fleet in self.get_idle_fleets(Ship.TROOP)])
		if not troop_count:
			return None

		colony = e.pop(0)
		defense = colony.get_defense()
		logging.debug("troops: %f, def: %f" % (troop_count, defense))
		if troop_count > defense > 1:
			return colony

		return None

	def assess_colonize(self):
		"""
		If there is an uninhabited world, and a colony ship,
		go ahead and colonize
		"""
		colonyships = sum([fleet.count for fleet in self.get_idle_fleets(Ship.COLONY)])
		if not colonyships:
			return None

		habitable_planets = sorted(self.get_empty_planets(), key=lambda p: p.cong_food, reverse=True)
		if not habitable_planets:
			return None

		to_take = min([len(habitable_planets), int(colonyships)])
		return habitable_planets[:to_take]

	def assess_navigate(self, mapper, api):
		"""
		Look at our fleets, to see if they can be moved.
		"""
		ls = []

		borders = Config.borders
		coords = self.get_coords()

		for ship_type, target_fn in Star.target_fns:
			cfg = Config.ships.get(ship_type, Star.ship_defaults)
			if not cfg.get("navigate", False):
				continue

			fleets = self.get_idle_fleets(ship_type)
			fleet = fleets[:1]

			if not fleet:
				continue

			fleet = fleet[0]

			# don't take colonyships if they are needed
			if fleet.type == Ship.COLONY:
				if len(self.planets) - len(self.colonies) > 0:
					continue

			# don't take troops if they are needed
			if fleet.type == Ship.TROOP and self.enemy_colonies:
					continue

			# scouts must have cloak
			if fleet.type == Ship.SCOUT and len(fleet.upgrades) < 1:
				continue

			if fleet.count < cfg.get("min_send", 1) and fleet.count > 0:
				continue

			possible_targets = target_fn(coords)
			max_distance = cfg.get("max_dist", 25)
			targets = []
			tether = cfg.get("tether")
			
			for target_star in possible_targets:
				target_coords = (target_star.get("x"), target_star.get("y"))

				if Mapper.in_no_fly_zone(target_coords, borders):
					continue

				distance = Mapper.distance(coords, target_coords)
				if distance > max_distance:
					continue

				tether_pass = True
				if tether:
					tether_pass = False
					nearby_colonies = Db.nearby_colonies(target_coords)
					if not nearby_colonies:
						continue

					for col_star in nearby_colonies:
						star_coords = (col_star.get("x"), col_star.get("y"))
						distance = Mapper.distance(coords, star_coords)
						if distance <= tether:
							tether_pass = True
							break

				if tether_pass:
					targets.append((distance, target_star))

			if not targets:
				continue

			targets = sorted(targets, key=lambda t: t[0])
			for next_closest in targets:

				distance, target_star = next_closest
				
				star_id = target_star.get("id")
				data = api.get_star(star_id)
				star_proto = warworld_pb2.Star()
				star_proto.ParseFromString(data)
				ts = Star(star_proto, record=True)
				if ts.has_friends():
					logging.info("skipping target because friend")
					continue

				# send just enough to take out the target
				if ship_type == Ship.TROOP:
					defense = target_star.get("defense") + 1

					if defense < fleet.count:
						api.split_fleet(self.data.id, ship_type, fleet, defense)

				# lacking colonies, leave 1 fighter behind
				if ship_type == Ship.FIGHTER:
					if len(self.colonies) == 0:
						api.split_fleet(self.data.id, ship_type, fleet, fleet.count - 1)

				# split to max-send-size if needed
				max_send = cfg.get("max_send")
				if fleet.count > max_send:
					api.split_fleet(self.data.id, ship_type, fleet, max_send)

				logging.debug("fleet %s targeting %s" % (fleet.id, target_star.get("name")))
				ls.append((fleet, target_star))
				break

		return ls

	def has_colonyship(self):
		"""
		True if the star has a colonyship on-hand or in the queue
		"""
		if self.colonyship_handled:
			return True

		# in orbit?
		fleet = self.get_idle_fleets(Ship.COLONY)
		if len(fleet):
			return True

		# in queue?
		c = [c for c in self.data.queue if c.type == Ship.COLONY]
		if len(c):
			return True

		return False

	def get_idle_fleets(self, ship_type):
		"""
		Get idle fleets of the supplied type
		"""
		return [fleet for fleet in self.fleets if fleet.type == ship_type and fleet.state == warworld_pb2.IDLE]

	def summary(self):
		name = self.get_name()
		x, y = self.get_coords()

		colonies = len(self.colonies)
		planets = len(self.planets)

		defense = 0
		for e in self.enemy_colonies:
			defense += e.get_defense()

		fighters = sum([f.count for f in self.get_idle_fleets(Ship.FIGHTER)])
		troops = sum([f.count for f in self.get_idle_fleets(Ship.TROOP)])
		colony_ships = sum([f.count for f in self.get_idle_fleets(Ship.COLONY)])

		max_pop, max_food = self.habitat_summary()
		return str(self.data.id), str(name), self.data.type, x, y, planets, colonies, defense, fighters, troops, colony_ships, max_pop, max_food, self.get_controlling_player()

	def habitat_summary(self):
		max_pop = 0
		max_food = 0
		for p in self.planets:
			max_pop += p.cong_pop + 400
			max_food += p.cong_food

		return max_pop, max_food

	def has_friends(self):
		return len(self.friendly_colonies or self.friendly_fleets) > 0

	def has_enemies(self):
		return len(self.enemy_colonies or self.enemy_fleets) > 0

	def get_human_enemies(self):
		e = [c for c in self.enemy_colonies if c.data.player != u'']
		return e

		#e = [f for f in self.enemy_fleets if f.player != u'']
		#return len(e) > 0

	def is_contested(self):
		return all((self.has_friends(), self.get_human_enemies()))

	def get_controlling_player(self):
		if self.colonies:
			return self.colonies[0].data.player

		if self.friendly_colonies:
			# TODO, most prolific friend over this random one?
			for fc in self.friendly_colonies:
				return fc.data.player

		e = self.get_human_enemies()
		if e:
			return e[0].data.player

		return 0



class Colony:

	"""
	Domain object for a planetary colony, consisting
	of buildings, a build queue, resource allocations,
	and a population of would-be stellar adventurers that
	will likely starve to death without this software.
	"""
	def __init__(self, colony_data):
		"""
		Initialize with the Colony protobuf message
		and some data borrowed from the Star message
		"""
		self.data = colony_data["colony"]
		self.queue = colony_data["queue"]
		self.buildings = colony_data["buildings"]
		self.planet = colony_data["planet"]
		self.focus = None
		self.is_solo = False

	def nominate_build_jobs(self):
		"""
		Follows a structure build pipeline, ordering jobs
		given their current conditions.
		Returns a list of 0 or more build job descriptors
		"""

		if Config.rush_enabled:
			rush_types = [] + Config.rush
			solo_rush = Config.solo_rush or []
			if self.is_solo:
				rush_types += solo_rush

			# already building something
			if rush_types and len(self.queue):
				rushes = []
				for job in self.queue:
					if job.type in rush_types:
						rushes.append(self.rush_job(job))
				if rushes:
						return rushes

		if self.queue:
			return None

		for building_type, level in Config.pipeline:
			if self.has_at_level(building_type, level, or_better=True):
				continue

			if self.type_in_queue(building_type):
				continue
			
			if self.has_building(building_type):
				b = self.get_building(building_type)
				if not b:
					logging.warn("we shouldn't be here..")
				else:
					return [self.building_to_job(b[0])]
			else:
				return [self.new_job(building_type)]


	def get_building(self, building_type, level=None):
		"""
		Get finished buildings of the supplied type,
		filtering by level if given
		"""
		ls = self.buildings
		ls = self.filter_building_type(ls, building_type)
		if level is not None:
			ls = self.filter_level(ls, level)
		ls = self.filter_queued(ls)

		return ls

	def has_building(self, building_type, level=None):
		"""
		True when the colony is in possession of a building
		of the supplied building_type (and level, if present)
		that is not under construction.
		"""

		return len(self.get_building(building_type, level)) > 0

	def has_any(self, building_type):
		"""
		True if the colony has a building of the supplied
		type finished or under construction.
		"""
		ls = [job for job in self.queue if job.type == building_type]
		if len(ls):
			return True

		ls = self.buildings
		ls = self.filter_building_type(ls, building_type)
		return len(ls) > 0

	def is_missing(self, building_type):
		"""
		True if the colony does not have a building
		of the supplied building_type. It's OK if it's in
		the build queue; that's likely why we're asking.
		"""
		return not self.has_any(building_type)

	@staticmethod
	def filter_level(building_list, level, or_better=False):
		"""
		Filter building_list checking its level with the argument
		"""
		if or_better:
			return [b for b in building_list if b.level >= level]
		else:
			return [b for b in building_list if b.level == level]

	@staticmethod
	def filter_building_type(building_list, building_type):
		"""
		Filter building_list checking its type with the argument
		"""
		return [b for b in building_list if b.type == building_type]

	@staticmethod
	def filter_maxed_level_buildings(building_list):
		"""
		Filter building_list removing those at their max level
		"""
		return [b for b in building_list if b.level < Building.max_levels[b.type]]

	def filter_queued(self, building_list):
		"""
		Filter building_list removing those under construction
		"""
		return [b for b in building_list if not self.in_queue(b)]

	def type_in_queue(self, building_type):
		"""
		Tests a building type for its presence in the build queue
		ie, is under construction
		"""
		for b in self.queue:
			if b.type == building_type:
				return True
		return False

	def in_queue(self, building):
		"""
		Tests a building for its presence in the build queue
		ie, is under construction
		"""
		for b in self.queue:
			if b.id == building.id:
				return True
		return False

	def has_at_level(self, building_type, level, or_better=False):
		"""
		True when the colony has at least one building
		of the supplied building_type and level
		either finished or queued
		"""
		ls = self.buildings
		ls = self.filter_building_type(ls, building_type)
		ls = self.filter_level(ls, level, or_better)
		return len(ls) > 0

	def get_upgrade(self, building_type):
		"""
		For the supplied building_type, attempt
		to locate a nominee for upgrade
		"""
		ls = self.buildings
		ls = self.filter_building_type(ls, building_type)
		ls = self.filter_maxed_level_buildings(ls)
		ls = self.filter_queued(ls)
		if ls:
			return ls.pop(0)
		else:
			return None

	def building_to_job(self, building):
		"""
		Generate a tuple describing an upgrade job
		"""
		return building.type, self, building.id, False

	def new_job(self, building_type):
		"""
		Generate a tuple describing a new-building job
		"""
		return building_type, self, "", False

	def rush_job(self, job):
		"""
		Generate a tuple describing a new-building job
		"""
		return job.type, self, job.job_id, True

	def nominate_ships(self, star):
		"""
		After some preliminary tests on buildings,
		decide if fleet ships should be ordered
		"""
		reqs = [
			(Building.SHIP, 1),
			(Building.BIO, Building.max_levels[Building.BIO])
		]

		req_pass = [self.has_building(btype, level) for btype, level in reqs]
		if not all(req_pass):
			return None

		# takeover
		idle_cs = star.get_idle_fleets(Ship.COLONY)
		if self.is_solo and not idle_cs:
			order = 1 #len(star.planets) - len(star.colonies)
			logging.info("star %s is alone, ordering %d CS" % (star.data.name, order))
			return [(self, [(Ship.COLONY, order)])]
		
		if len(self.queue) > 0:
			return None

		if self.data.focus_build < .5:
			return None

		fleet_orders = []
		for ship_type in Config.ships:
			cfg = Config.ships.get(ship_type, Star.ship_defaults)
			build_count = cfg.get("build", 0)
			max = cfg.get("build_max")

			# cap building at the maximum, if one was set
			if isinstance(max, (int, long)):
				ship_sum = sum([f.count for f in star.get_idle_fleets(ship_type)])
				build_count = int(min(build_count, max - ship_sum))
			
				if build_count < 1:
					continue

			fleet_orders.append((ship_type, build_count))
			if ship_type == Ship.COLONY:
				star.colonyship_handled = True

		return [(self, fleet_orders)]

	def get_defense(self):
		"""
		Calculate a colony's total defense value
		"""
		base_defense = self.data.cur_pop / 4
		shield_defense = base_defense / 4
		shield = self.get_building(Building.SHIELD)
		if shield:
			base_defense += shield_defense * shield.level

		return base_defense

	def get_ratio(self, statA, statB):
		res_map = {
			Resource.POP: self.planet.cong_pop,
			Resource.MINE: self.planet.cong_mine,
			Resource.FOOD: self.planet.cong_food
		}

		statA = res_map.get(statA)
		statB = res_map.get(statB)

		if statB:
			return statA / statB
		return statA


class ResourceAllocation:
	"""
	Divides a population among the game's 4 tasks:
	population, food, mining, building.

	Converts population counts into floats 0 <= n <= 1
	"""
	def __init__(self, pop):
		"""
		Initialize with the colony's current population
		"""
		# unchanging
		self.pop = float(pop)
		self.unallocated = None

		# assignments
		self.allocations = {}
		self.reset()

	def reset(self):
		self.unallocated = self.pop
		self.allocations = {
			Resource.POP: 0.0,
			Resource.FOOD: 0.0,
			Resource.MINE: 0.0,
			Resource.BUILD: 0.0
		}

	def allocate(self, task, pop):
		"""
		Allocate a number of population to a task.
		Population is a float
		"""

		# bounded by our limits
		pop = min([pop, self.unallocated])
		pop = max([pop, 0])
		self.allocations[task] += pop
		self.unallocated -= pop

	def allocate_remaining(self, task):
		"""
		Assign all unassigned population to a task
		"""
		self.allocate(task, self.unallocated)

	def to_floats(self):
		"""
		Convert population numbers to floats
		totalling 1.0
		"""
		parts = {}
		for k in self.allocations:
			parts[k] = self.part(self.allocations[k])
		return parts

	def part(self, pop):
		"""
		Return pop's part of the whole (1.0)
		"""
		return pop / self.pop


class Sector:
	"""
	A collection of stars within bounding box.
	Provided by map requests
	"""
	def __init__(self, data):
		self.x = data.x
		self.y = data.y
		self.stars = [Star(s) for s in data.stars]


class Alliance:
	"""
	An alliance descriptor
	"""
	def __init__(self, data, record=True):
		self.id = data.id
		self.name = data.name
		self.member_count = data.member_count
		self.bank = data.bank
		self.members = data.members

		if record:
			Db.update_alliance(self.summary())

	def summary(self):
		return self.id, self.name, self.member_count, self.bank

