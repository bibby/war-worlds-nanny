import logging
from math import sqrt
import itertools


class Mapper:
	@staticmethod
	def distance(a, b, parsecs=True):
		ax, ay = a
		bx, by = b
		mult = 100 if parsecs else 1
		return abs(sqrt((bx - ax) ** 2 + (by - ay) ** 2)) * mult

	@staticmethod
	def sector_range(center, r):
		cx, cy = center
		ls = []
		for x in range(cx - r, cx + r + 1):
			for y in range(cy - r, cy + r + 1):
				ls.append((x, y))

		return ls

	@staticmethod
	def groups_of(n, iterable, fillvalue=None):
		"""grouper(3, 'ABCDEFG', 'x') --> ABC DEF Gxx"""
		args = [iter(iterable)] * n
		return itertools.izip_longest(*args, fillvalue=fillvalue)

	@staticmethod
	def map_sectors(mapFn, api, sector, radius=0):
		groups = Mapper.groups_of(12, Mapper.sector_range(sector, radius))
		for sectors in groups:
			map(mapFn, Mapper.fetch_sectors(sectors, api))

	@staticmethod
	def fetch_sectors(sectors, api):
		logging.info("Fetching sector %s", sectors)
		try:
			response = api.load_sectors(sectors)

			for sector in response:
				for star in sector.stars:
					yield star.summary()
		except:
			logging.info("Failed fetching sector %s", sectors)
			

	@staticmethod
	def point_in_triangle(point, triangle):
		p = Point(point)
		p0, p1, p2 = map(Point, triangle)

		a = 1.0 / 2 * (-p1.y * p2.x + p0.y * (-p1.x + p2.x) + p0.x * (p1.y - p2.y) + p1.x * p2.y)
		sign = -1 if a < 0 else 1
		s = (p0.y * p2.x - p0.x * p2.y + (p2.y - p0.y) * p.x + (p0.x - p2.x) * p.y) * sign
		t = (p0.x * p1.y - p0.y * p1.x + (p0.y - p1.y) * p.x + (p1.x - p0.x) * p.y) * sign
		return s > 0 and t > 0 and (s + t) < 2 * a * sign

	@staticmethod
	def point_in_shape(point, shape):
		for triangle in itertools.combinations(shape, 3):
			if Mapper.point_in_triangle(point, triangle):
				return True
		return False

	@staticmethod
	def in_no_fly_zone(point, zones):
		for shape in zones:
			if Mapper.point_in_shape(point, shape):
				return True
		return False

class Point:
	def __init__(self, xy):
		self.x, self.y = xy
