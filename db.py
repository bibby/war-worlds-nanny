from __builtin__ import staticmethod
import logging
import sqlite3
from consts import Ship


class Db:
	"""
	Sqlite navdb client
	"""
	conn = sqlite3.connect('nanny.db')
	silent = True
	target_spec = ""

	send_callback = {}

	@staticmethod
	def init(silent=True):
		Db.silent = silent
		Db.conn.row_factory = Db.dict_row_factory
		sql = open("sql/schema.sql", "r")
		Db.conn.executescript(sql.read())

		Db.target_spec = open("sql/target.sql", "r").read()
		Db.send_callback = {
			Ship.FIGHTER: Db.fighters_sent,
			Ship.TROOP: Db.troops_sent,
			Ship.COLONY: Db.colonyships_sent,
			Ship.SCOUT: Db.scout_sent
		}

	@staticmethod
	def dict_row_factory(cursor, row):
		"""
		Returns rows as dicts
		"""
		d = {}
		for index, col in enumerate(cursor.description):
			d[col[0]] = row[index]
		return d

	@staticmethod
	def exec_and_close(stmt, args=[]):
		"""
		Run a statement then close the cursor
		"""
		c = Db.conn.cursor()
		c.execute(stmt, args)
		Db.conn.commit()

	@staticmethod
	def add_stars(*stars):
		cursor = Db.conn.cursor()
		for star in stars:
			Db.add_star(cursor, *star)
		Db.conn.commit()

	@staticmethod
	def add_star(cursor, *record):
		"""
		Add or replace a record.
		Leaves the cursor open for more
		"""
		if not Db.silent:
			print "Recording %s - %s" % tuple(record[:2])

		placeholders = ["?"] * len(record)
		stmt = "INSERT OR REPLACE INTO stars VALUES (%s)" % ",".join(placeholders)
		cursor.execute(stmt, tuple(record))

		stmt = "INSERT OR IGNORE INTO actions(id) VALUES (?)"
		cursor.execute(stmt, tuple([record[0]]))

	@staticmethod
	def update_star(star):
		cursor = Db.conn.cursor()
		logging.info("Updating nav record for %s - %s" % (star[:2]))
		Db.add_star(cursor, *star)
		Db.conn.commit()

	@staticmethod
	def add_alliance(cursor, *record):
		"""
		Add or replace a record.
		Leaves the cursor open for more
		"""
		if not Db.silent:
			print "Recording %s - %s" % tuple(record[:2])

		placeholders = ["?"] * len(record)
		stmt = "INSERT OR REPLACE INTO alliances VALUES (%s)" % ",".join(placeholders)
		cursor.execute(stmt, tuple(record))

	@staticmethod
	def update_alliance(alliance):
		cursor = Db.conn.cursor()
		logging.info("Updating alliance record for %s - %s" % (alliance[:2]))
		Db.add_alliance(cursor, *alliance)
		Db.conn.commit()

	@staticmethod
	def add_player(cursor, player):
		"""
		Add or replace a record.
		Leaves the cursor open for more
		"""
		if not Db.silent:
			print "Recording %s - %s" % (player.id, player.name)

		record = (
			player.id,
			player.name,
			player.alliance.id,
			player.summary.rank,
			player.summary.stars,
			player.summary.colonies,
			player.summary.ships,
			player.summary.population
		)

		placeholders = ["?"] * len(record)
		stmt = "INSERT OR REPLACE INTO players VALUES (%s)" % ",".join(placeholders)
		cursor.execute(stmt, tuple(record))

	@staticmethod
	def update_player(player):
		cursor = Db.conn.cursor()
		logging.info("Updating player record for %s - %s" % (player.id, player.name))
		Db.add_player(cursor, player)
		Db.conn.commit()

	@staticmethod
	def fighter_target(position):
		"""
		A set of clauses appropriate for fighters
		"""
		clauses = ["fighters_sent = 0", "colonies = 0", "planets > 0", "type != 8"]
		return Db.__target(position, clauses)

	@staticmethod
	def troop_target(position):
		"""
		A set of clauses appropriate for troop carriers
		"""
		clauses = ["defense - troops_sent > 0", "fighters_sent > 0", "type != 8"]
		return Db.__target(position, clauses)

	@staticmethod
	def colony_target(position):
		"""
		A set of clauses appropriate for colony ships
		"""
		clauses = ["colonies + colonyships_sent = 0", "(fighters > 0 or fighters_sent > 0)", "type != 8"]
		return Db.__target(position, clauses)

	@staticmethod
	def scout_target(position):
		"""
		A set of clauses appropriate for scouts
		"""
		clauses = ["not scouted"]
		return Db.__target(position, clauses)

	@staticmethod
	def nearby_colonies(position):
		clauses = ["colonies > 0"]
		return Db.__target(position, clauses)

	@staticmethod
	def __target(position, clauses=[]):
		"""
		Common targeting method.
		"""
		clauses = clauses + ["abs( x - ? ) < 1", "abs( y - ? ) < 1"]
		x, y = position
		args = [x, y]

		stmt = Db.target_spec % " AND ".join(clauses)
		logging.debug("SQL: %s [%s]" % (stmt, args))
		return Db.conn.cursor().execute(stmt, args)

	@staticmethod
	def fighters_sent(count, star):
		Db.update_action("fighters_sent", count, star)

	@staticmethod
	def troops_sent(count, star):
		Db.update_action("troops_sent", count, star)

	@staticmethod
	def colonyships_sent(count, star):
		Db.update_action("colonyships_sent", count, star)

	@staticmethod
	def scout_sent(count, star):
		Db.update_action("scouted", 1, star)
	
	@staticmethod
	def update_action(field, count, star):
		stmt = "update actions set %s = %s + ? where id = ?" % (field, field)
		args = [count, star]
		logging.debug("SQL: %s [%s]" % (stmt, args))
		return Db.exec_and_close(stmt, args)

	@staticmethod
	def get_sectors(sectors):
		stars = []
		for sector in sectors:
			for star in Db.get_sector(sector):
				stars.append(star)
		return stars

	@staticmethod
	def get_sector(sector):
		stmt = "SELECT * from stars where cast(x as int) = ? and cast(y as int) = ?"
		c = Db.conn.cursor()
		return c.execute(stmt, sector)
