"""
War Nanny - The baby-sitter for War Worlds on Android.
Main Startup script.

Usage:
	map.py [-ch] --x=<X> --y=<Y> R S

Options:
	-h --help		Show this screen
	-c				Use cache. Don't contact game server
"""
import os
import errno
from time import time

from docopt import docopt

from mapgen import MapGen
from war_nanny import API


def mkdir_p(path):
	try:
		os.makedirs(path)
	except OSError as exc: # Python >2.5
		if exc.errno == errno.EEXIST and os.path.isdir(path):
			pass
		else:
			raise


def run(args):
	start = time()
	api = API()
	cached = args.get("-c", False)
	fetch = False
	if not cached:
		fetch = True
		api.authenticate()

	fpath = "/var/www/html/maps"
	x = int(args.get("--x", 0))
	y = int(args.get("--y", 0))
	r = int(args.get("R") or 1)
	size = int(args.get("S") or 15)
	mkdir_p(fpath)
	fname = "%s/x%s_y%s_r%s__%s.png" % (fpath, x, y, r, int(time()))

	print "generating %s.." % fname
	print MapGen.draw_sector(api, (x, y), r, fname=fname, size=size, fetch=fetch)
	end = time()
	print "finished in %.2f" % (end - start)

if __name__ == '__main__':
	version = 'MapNanny 0.02 [Kar]'
	arguments = docopt(__doc__, version=version)
	print arguments
	run(arguments)
